﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        : BaseEntity
    {
        [MaxLength(256)]
        public string FirstName { get; set; }
        [MaxLength(256)]
        public string LastName { get; set; }

        [MaxLength(512)]
        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(256)]
        public string Email { get; set; }

        public virtual ICollection<CustomerPreference> CustomerPreferences { get; set; }
        public virtual ICollection<PromoCode> PromoCodes { get; set; }
    }
}