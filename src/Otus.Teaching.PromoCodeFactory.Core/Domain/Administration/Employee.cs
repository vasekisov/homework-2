﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        [MaxLength(256)]
        public string FirstName { get; set; }

        [MaxLength(300)]
        public string LastName { get; set; }

        [MaxLength(526)]
        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(256)]
        public string E_mail { get; set; }

        public Guid RoleId { get; set; }
        public virtual Role Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}