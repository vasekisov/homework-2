﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.E_mail,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if(employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.E_mail,
                Role = new RoleItemResponse()
                {
                    Name = employee.Role.Name,
                    Description = employee.Role.Description
                },
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
        [HttpPost]
        public async Task<ActionResult<EmployeeRequest>> Add(EmployeeRequest employeeRequest)
        {
            if(employeeRequest == null)
            {
                return BadRequest();
            }

            var newEmployee = new Employee()
            {
                Id = Guid.NewGuid(),
                E_mail = employeeRequest.Email,
                FirstName = employeeRequest.FirstName,
                LastName = employeeRequest.LastName,
                AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount,

            };
            var result = _employeeRepository.AddAsync(newEmployee);

            return await Task.FromResult(Ok());
        }

        /// <summary>
        /// Обновить данные конкретного сотрудника
        /// </summary>
        /// <param name="employeeResponse"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<EmployeeRequest>> Edit(Employee employeeResponse)
        {
            var result = _employeeRepository.UpdateByIdAsync(employeeResponse);

            return await Task.FromResult(Ok());
        }

        /// <summary>
        /// Удаление сотрудника по его id
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<EmployeeResponse>> Delete(Employee data)
        {
            var result = _employeeRepository.DeleteByIdAsync(data);

            return await Task.FromResult(Ok());
        }
    }
}