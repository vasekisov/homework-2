﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T>
      : IRepository<T>
        where T : BaseEntity
    {
        private readonly DataContext _dataContext;
        public EfRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dataContext.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }
        public async Task DeleteByIdAsync(T data)
        {
            _dataContext.Set<T>().Remove(data);
            await _dataContext.SaveChangesAsync();
        }
        public async Task UpdateByIdAsync(T data)
        {
            await _dataContext.SaveChangesAsync();
        }
        public async Task AddAsync(T data)
        {
            await _dataContext.Set<T>().AddAsync(data);
            await _dataContext.SaveChangesAsync();
        }

    }
}
