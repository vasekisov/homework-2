﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }
        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }
        public Task DeleteByIdAsync(T guid)
        {
            var DataList = (ICollection<T>)Data;
            DataList.Remove(Data.FirstOrDefault(e => e.Id == guid.Id));

            return Task.FromResult(true);
        }
        public Task UpdateByIdAsync(T data)
        {
            var DataList = (ICollection<T>)Data;
            var editElem = DataList.FirstOrDefault(e => e.Id == data.Id);

            DataList.Remove(editElem);
            DataList.Add(data);

            return Task.FromResult(true);
        }
        public Task AddAsync(T data)
        {
            var dataList = (ICollection<T>)Data;
            dataList.Add(data);
            
            return Task.FromResult(true);
        }
    }
}